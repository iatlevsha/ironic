# Ironic image

This is the private build of [OpenStack Ironic](https://opendev.org/openstack/ironic/) service to be used with Metal3.

Metal3 contains its [own docker image of Ironic](https://github.com/metal3-io/ironic-image),
though that image uses configuration scripts made for a very specific usage scenario: provisioning using PXE.

This image is an attempt to make the image primary for Virtual Media provisioning.

## Configuration

Configuration is imported from these places:

* environment variables in the form OS\_{SECTION\_NAME\_IN\_UPPERCASE}\_\_{VALUE\_NAME\_IN\_UPPERCASE}
* configuration files from the directory set by the '--config-dir DIR' flag
* configuration file set by the '--config-file PATH' flag

A value set in an environment variable takes precedence over configuration files.

Configuration files from the directory set by the '--config-dir' flag take precedence over the configuration file set by the '--config-file' flag.

Configuration files from the directory set by the '--config-dir' flag are ordered alphabetically and later files take precedence over earlier ones.

The image contains a basic /etc/defaults/ironic.conf that can be used as the value of '--config-file'
