# WIP: Provision using iDRAC virtual media interface over L3 network. No DHCP needed.

1. Deploy Ironic using this [kustomize.yaml](kustomize.yaml)
1. Deploy Cluster API with Metal3 master. TODO: describe how to deploy Metal3 master.
1. Patch Metal3 operator to use [quay.io/levsha/baremetal-operator:no-inspector](https://quay.io/repository/levsha/baremetal-operator?tab=tags) image that was built from [https://github.com/levsha/baremetal-operator/tree/no-inspector](https://github.com/levsha/baremetal-operator/tree/no-inspector)

You can use this definiton to boot Fedora CoreOS live image now:

```yaml
apiVersion: metal3.io/v1alpha1
kind: BareMetalHost
metadata:
  name: host01
spec:
  bootMACAddress: XX:XX:XX:XX:XX:XX # MAC address of the host (not BMC) interface.
  bmc:
    address: idrac-virtualmedia://10.20.30.40/redfish/v1/Systems/System.Embedded.1
    credentialsName: host01-idrac-secret
    disableCertificateVerification: true
  image:
    url: https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/33.20210117.3.2/x86_64/fedora-coreos-33.20210117.3.2-live.x86_64.iso
    format: live-iso
  online: true
```
