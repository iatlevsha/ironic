
CONTAINERS	= $(subst /,,$(dir $(wildcard */Makefile)))

.PHONY: all
all: build

.PHONY: build
build: $(CONTAINERS)

.PHONY: $(CONTAINERS)
$(CONTAINERS):
	docker build $@ --tag $@:dirty
