FROM alpine:3.14.2

RUN apk add \
  build-base \
  ipmitool \
  python3-dev \
  py3-alembic \
  py3-bcrypt \
  py3-cryptography \
  py3-greenlet \
  py3-lxml \
  py3-mako \
  py3-markupsafe \
  py3-netifaces \
  py3-pip \
  py3-prettytable \
  py3-psutil \
  py3-pyrsistent \
  py3-pysendfile \
  py3-simplejson \
  py3-sqlalchemy \
  py3-tempita \
  py3-yaml \
  py3-wheel \
  py3-wrapt \
  sqlite \
  xorriso

RUN pip3 install --no-cache-dir \
  ironic==18.2.0 \
  ironic-inspector \
  python-dracclient \
  python-ibmcclient \
  sushy \
  sushy-oem-idrac

COPY ironic.conf ironic-inspector.conf etc/defaults/

RUN mkdir -p /var/lib/ironic-inspector && sqlite3 /var/lib/ironic-inspector/ironic-inspector.db "pragma journal_mode=wal"

COPY patches /tmp/patches
RUN cd $(pip3 show ironic | awk '/^Location:/ {print $2}') && \
  cat /tmp/patches/*.patch | patch -p0
RUN rm -r /tmp/patches
